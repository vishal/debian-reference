#######################################################################
# Build debian-reference (v2)
# vim: set ts=8:
#######################################################################
### key adjustable parameters
#######################################################################
# base file name excluding file extension
MANUAL	:=	debian-reference
# languages translated with PO files
# This can list more than debian/rules for translator to check the result
LANGPO	:=	ja fr it pt de zh-cn zh-tw es id nb
# languages to skip generation of PDF files (not used now)
NOPDF	:=	
# languages to build document
LANGALL	=	en $(LANGPO)

# Change at the last moment (this package is part of web page)
# from testing -> stable just before stable release

# Update release name when setting back RELEASE_STATE
# from stable -> testing just after stable release

# This is normal
#RELEASE_STATE := testing

# this is just before freeze
RELEASE_STATE := stable

# TMP_DIR_DR is not mean to be set to generic TMPDIR like /tmp ~/.tmp
ifndef TMP_DIR_DR
TMP_DIR_DR	:= $(CURDIR)/tmp
endif

# Change $(DRAFTMODE) from "yes" to "maybe" when this document 
# should go into production mode
#DRAFTMODE      := yes
DRAFTMODE       := maybe
export DRAFTMODE
#######################################################################
### basic constant parameters
#######################################################################
# Directories (no trailing slash)
DXSL	:=	xslt
DBIN	:=	bin
DASC	:=	asciidoc
DPO	:=	po
DIMG	:=	/usr/share/xml/docbook/stylesheet/nwalsh/images

# work around https://bugs.debian.org/725931
PERL_PERTURB_KEYS := 0
export PERL_PERTURB_KEYS
PERL_HASH_SEED := 0
export PERL_HASH_SEE

# Program name and option
XLINT	:=	xmllint --format
XPNO	:=	xsltproc --novalid --nonet
XPINC	:=	xsltproc --novalid --nonet --xinclude
GETTEXT	:=	po4a-gettextize -M utf-8 -L utf-8 --format docbook
UPDATEPO:=	msgmerge --update --previous
MSGATTR	:=	msgattrib
MSGCAT	:=	msgcat
DBLATEX	:=	dblatex

# Debian package archive URL
#DEBM	:=	http://ftp.us.debian.org/debian/dists
#DEBM	:=	http://ftp.jp.debian.org/debian/dists
DEBM	:=	http://deb.debian.org/debian/dists
# Debian popcon data source URL
UPOPC	:=	http://popcon.debian.org/all-popcon-results.txt.gz
# Debian release name and arch used
CODE	:=	sid
ARCH	:=	amd64
UDEBA	:=	$(DEBM)/$(CODE)
UDEBB	:=	$(DEBM)/experimental
UDEBC	:=	$(DEBM)/stable
UDEBD	:=	$(DEBM)/oldstable
DR_VERSION :=	$(shell dpkg-parsechangelog --show-field Version)

# AsciiDoc source file names in $(DASC) directories for local update
ASC_NM	:=	00_preface.txt 01_tutorial.txt 02_package.txt \
		03_sysinit.txt 04_auth.txt 05_network.txt 06_netapp.txt \
		07_xwindow.txt 08_i18nl10n.txt 09_systips.txt 10_datamngt.txt \
		11_dataconv.txt 12_program.txt 99_appendix.txt \
		copyright.txt header1.txt header2.txt $(MANUAL).txt
# source asciidoc files (local asciidoc conversion script used for stability)
SRC_ASC :=	$(addprefix $(DASC)/, $(ASC_NM))

#######################################################################
# Used as $(call check-command, <command>, <package>)
define check-command
set -e; if ! which $(1) >/dev/null; then \
  echo "Missing command: $(1), install package: $(2)"; \
  false; \
fi
endef
#######################################################################
# $ make all       # build all
#######################################################################
.PHONY: all
# set LANGPO to limit language to speed up build
all: css html txt epub pdf
	cat fuzzy.log

#######################################################################
# $ make test      # build html for testing (for Translator)
#######################################################################
.PHONY: test
test: html css

#######################################################################
# Phase 1 build : fetch remote data
#######################################################################
# cached data removed for package related to remotely fetched data
REMOTE_DATA :=	packages.main packages.contrib packages.non-free \
		packages.txt packages.bkup.txt all-popcon-results.txt \
		all-popcon-submissions.txt all-popcon-pkgs.txt pkg.lst
.PHONY: remote
remote: $(REMOTE_DATA)

packages.main:
	# FETCH PACKAGE (sid main)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBA)/main/binary-$(ARCH)/Packages.xz      | xzcat - > packages.main.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.main.tmp > packages.main
	rm packages.main.tmp

packages.contrib:
	# FETCH PACKAGE (sid contrib)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBA)/contrib/binary-$(ARCH)/Packages.xz   | xzcat - > packages.contrib.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.contrib.tmp > packages.contrib
	rm packages.contrib.tmp

packages.non-free:
	# FETCH PACKAGE (sid non-free)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBA)/non-free/binary-$(ARCH)/Packages.xz  | xzcat - > packages.non-free.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.non-free.tmp > packages.non-free
	rm packages.non-free.tmp

packages.txt: packages.main packages.contrib packages.non-free
	# FETCH PACKAGE (sid)
	cat packages.main packages.contrib packages.non-free >packages.txt

packages.bkup.txt:
	# FETCH PACKAGE (experimental main)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBB)/main/binary-$(ARCH)/Packages.xz      | xzcat - > packages.bkup.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.bkup.tmp > packages.bkup.txt
	rm packages.bkup.tmp

packages.stable.txt:
	# FETCH PACKAGE (stable main)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBC)/main/binary-$(ARCH)/Packages.xz      | xzcat - > packages.stable.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.stable.tmp > packages.stable.txt
	rm packages.stable.tmp

packages.oldstable.txt:
	# FETCH PACKAGE (oldstable main contrib)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBD)/main/binary-$(ARCH)/Packages.xz      | xzcat - > packages.oldstable.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.oldstable.tmp > packages.oldstable.txt
	rm packages.oldstable.tmp
	wget -O - $(UDEBD)/contrib/binary-$(ARCH)/Packages.xz      | xzcat - > packages.oldstable.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.oldstable.tmp >> packages.oldstable.txt
	rm packages.oldstable.tmp

all-popcon-results.txt:
	# POPCON RESULTS
	wget -O - $(UPOPC) | zcat - > all-popcon-results.txt

all-popcon-submissions.txt: all-popcon-results.txt
	sed -n -e 's/^Submissions: *\([^ ]*\) *$$/\1/p' < all-popcon-results.txt >all-popcon-submissions.txt

all-popcon-pkgs.txt: all-popcon-results.txt
	grep --binary-files=text -e '^Package: [a-z0-9][-+a-z0-9.]*[ 0-9]*$$' < all-popcon-results.txt >all-popcon-pkgs.txt

#######################################################################
# Phase 2 build : package glob replacement script
#######################################################################

$(DBIN)/replace_package: packages.txt
	# GENERATE $(DBIN)/replace_package (sed script)
	echo "# This is autogenerated by Makefile.  Update this with \"make rawxml\"" \
		 >  $(DBIN)/replace_package
	$(DBIN)/genreplace libpoppler	>>  $(DBIN)/replace_package
	$(DBIN)/genreplace libgstreamer	>>  $(DBIN)/replace_package

#######################################################################
# Phase 3 build : ASCIIDOC -> RAW XML (with @emacs@ etc.)
#######################################################################

$(MANUAL).raw.xml: $(SRC_ASC) $(DBIN)/replace_package
	# BUILD RAWXML from ASCIIDOC
	$(DBIN)/asciidoc -a 'newline=\n' -o - $(DASC)/$(MANUAL).txt |\
	sed -e "/<\/author>/r $(DASC)/copyright.txt" |\
	$(XLINT) -  |\
	sed -f $(DBIN)/replace_package |\
	sed -f $(DBIN)/replace > $(MANUAL).raw.xml

#######################################################################
# Phase 4 build : RAW XML (with @emacs@ etc.) -> pkg.lst 
#######################################################################

pkg.lst: $(MANUAL).raw.xml
	@$(call check-command, xsltproc, xsltproc)
	# PACKAGE LIST of packages mentioned in the source XML
	$(XPNO) $(DXSL)/pkg.xsl $(MANUAL).raw.xml > pkg.lst

#######################################################################
# Phase 5 build : ENTITY
#######################################################################
# source XML inclusion files (excluding common.ent)
ENT_STAT:=	datadatepop.ent datadatesize.ent popcon.ent pkgsize.ent common.ent

datadatesize.ent: packages.txt packages.main packages.contrib packages.non-free
	# GENERATE datesize.ent
	echo "<!ENTITY all-packages \"$$(grep -e '^Package:' packages.txt | wc -l)\">"			>  datadatesize.ent
	echo "<!ENTITY main-packages \"$$( grep -e '^Package:' packages.main | wc -l)\">"		>> datadatesize.ent
	echo "<!ENTITY contrib-packages \"$$(grep -e '^Package:' packages.contrib | wc -l)\">"		>> datadatesize.ent
	echo "<!ENTITY non-free-packages \"$$(grep -e '^Package:' packages.non-free | wc -l)\">"	>> datadatesize.ent
	echo "<!ENTITY pkgsize-date \"$(shell date -u +'%F %T %Z')\">"					>> datadatesize.ent


datadatepop.ent:
	# GENERATE datadatepop.ent
	echo "<!ENTITY pop-date \"$(shell date -u +'%F %T %Z')\">" > datadatepop.ent

popcon.ent: all-popcon-results.txt all-popcon-pkgs.txt all-popcon-submissions.txt pkg.lst
	# GENERATE datadatepop.ent
	echo "<!ENTITY pop-submissions \"$$(sed -n -e 's/^Submissions: *\([^ ]*\) *$$/\1/p' < all-popcon-results.txt)\">"	>  popcon.ent
	echo "<!ENTITY pop-architectures \"$$(grep --binary-files=text -e '^Architecture:' all-popcon-results.txt | wc -l)\">"	>> popcon.ent
	echo "<!ENTITY pop-packages \"$$(grep -e '^Package:' all-popcon-pkgs.txt | wc -l)\">"				>> popcon.ent
	grep -e '^Package:' all-popcon-pkgs.txt | grep -f pkg.lst | $(DBIN)/popconent `cat all-popcon-submissions.txt`	>> popcon.ent

pkgsize.ent: pkg.lst packages.txt packages.bkup.txt packages.stable.txt packages.oldstable.txt
	# GENERATE pkgsize.ent
	sort pkg.lst | uniq | $(DBIN)/sizeent packages.txt packages.bkup.txt packages.stable.txt packages.oldstable.txt > pkgsize.ent

	# POPCON
	wget -O - $(UPOPC) | zcat - > all-popcon-results.txt
	sed -n -e 's/^Submissions: *\([^ ]*\) *$$/\1/p' < all-popcon-results.txt >all-popcon-submissions.txt
	grep --binary-files=text -e '^Package: [a-z0-9][-+a-z0-9.]*[ 0-9]*$$' < all-popcon-results.txt >all-popcon-pkgs.txt

common.ent:
	# GENERATE common.ent
	# Release for package match with actual situation
	echo "<!ENTITY build-date \"$(shell date -u +'%F %T %Z')\">"	>  common.ent
	echo "<!ENTITY arch \"$(ARCH)\">"				>> common.ent
	echo "<!ENTITY dr-version \"$(DR_VERSION)\">"			>> common.ent
ifeq ($(RELEASE_STATE),stable)
	# Aim to prepare for the upcoming stable release from sid/unstable
	echo "<!ENTITY codename-stable  \"buster\">"			>> common.ent
	echo "<!ENTITY Codename-stable  \"Buster\">"			>> common.ent
	echo "<!ENTITY codename-testing  \"bullseye\">"			>> common.ent
	echo "<!ENTITY Codename-testing  \"Bullseye\">"			>> common.ent
	echo "<!ENTITY codename-nexttesting  \"bookworm\">"		>> common.ent
	echo "<!ENTITY Codename-nexttesting  \"Bookworm\">"		>> common.ent
else
	# Live with 2 year testing release from sid/unstable
	echo "<!ENTITY codename-stable  \"stretch\">"			>> common.ent
	echo "<!ENTITY Codename-stable  \"Stretch\">"                   >> common.ent
	echo "<!ENTITY codename-testing  \"buster\">"			>> common.ent
	echo "<!ENTITY Codename-testing  \"Buster\">"			>> common.ent
	echo "<!ENTITY codename-nexttesting  \"bullseye\">"		>> common.ent
	echo "<!ENTITY Codename-nexttesting  \"Bullseye\">"		>> common.ent
endif
	echo "<!ENTITY codename-unstable \"sid\">"			>> common.ent
	echo "<!ENTITY Codename-unstable \"Sid\">"                      >> common.ent

#######################################################################
# Phase pre-6 pre-build : when updating English asciidoc, run this and commit it
#######################################################################
.PHONY: entity
entity: 
	$(MAKE) distclean
	$(MAKE) $(ENT_STAT)
	$(MAKE) clean
	# PLEASE MAKE SURE TO COMMIT TO VCS

#######################################################################
# Phase 6 build : XML ENGLISH (with template XML) -- NORMAL BUILD
#######################################################################

# Update URL list header from $(MANUAL).raw.xml
header.txt: $(DASC)/header1.txt $(DASC)/header2.txt
	# SANITY CHECK
	[ -f $(MANUAL).raw.xml ]
	# GENERATE header.txt
	cat $(DASC)/header1.txt >  header.txt
	$(XPNO) $(DXSL)/urls.xsl $(MANUAL).raw.xml | sort | uniq |\
	sed -e "s/&/\&amp;/g"   >> header.txt
	cat $(DASC)/header2.txt >> header.txt

# Replace table contents with @-@popcon*@-@ and @@@psize*@-@ and 
# fix URL referencees and table ID.
$(MANUAL).en.xml: header.txt
	# SANITY CHECK
	[ -f $(MANUAL).raw.xml ]
	# GENERATE $(MANUAL).en.xml
	@$(call check-command, xsltproc, xsltproc)
	# use asciidoc generated xml file as main contents 
	$(XPNO) $(DXSL)/table.xsl $(MANUAL).raw.xml |\
	$(DBIN)/colspec.py  |\
	sed -e '/<!DOCTYPE /d' -e "1r header.txt" |\
	sed -e 's/@-@amp@-@/\&/g' -e 's/@-@\([^@]\+\)@-@/\&\1;/g' > $@

# Replace table contents with dummy text and 
# fix URL referencees and table ID as the template for translation.
# This avoids bloated PO/POT files. (tablet.xsl used insted of table.xsl)
$(MANUAL).en.xmlt: header.txt
	# SANITY CHECK
	[ -f $(MANUAL).raw.xml ]
	# GENERATE $(MANUAL).en.xmlt (TEMPLATE to avoid bloated PO/POT files)
	@$(call check-command, xsltproc, xsltproc)
	# use asciidoc generated xml file as main contents 
	$(XPNO) $(DXSL)/tablet.xsl $(MANUAL).raw.xml |\
	$(DBIN)/colspec.py  |\
	sed -e '/<!DOCTYPE /d' -e "1r header.txt" |\
	sed -e 's/@-@amp@-@/\&/g' -e 's/@-@\([^@]\+\)@-@/\&\1;/g' > $@

#######################################################################
# Phase 7 build : POT/PO/XML non-ENGLISH (with template XML)
#######################################################################
# source PO files for all languages (build prcess requires these)
SRC_PO	:=	$(addsuffix .po, $(addprefix  $(DPO)/, $(LANGPO)))
.PHONY: po pot
pot: $(DPO)/templates.pot
po: $(SRC_PO)

# Do not record line number to avoid useless diff in po/*.po files: --no-location
# Do not update templates.pot if contents are the same as before; -I '^"POT-Creation-Date:'
$(DPO)/templates.pot: $(MANUAL).en.xmlt FORCE
	@$(call check-command, po4a-gettextize, po4a)
	@$(call check-command, msgcat, gettext)
	$(GETTEXT) -m $(MANUAL).en.xmlt | \
sed -e 's,^"Content-Type: text/plain; charset=CHARSET\\n"$$,"Content-Type: text/plain; charset=UTF-8\\n",' |\
	$(MSGCAT) --no-location -o $(DPO)/templates.pot.new -
	if diff -I '^"POT-Creation-Date:' -q $(DPO)/templates.pot $(DPO)/templates.pot.new ; then \
	  echo "Don't update templates.pot" ;\
	  touch $(DPO)/templates.pot ;\
	  rm -f $(DPO)/templates.pot.new ;\
	else \
	  echo "Update templates.pot" ;\
	  mv -f $(DPO)/templates.pot.new $(DPO)/templates.pot ;\
	fi
	: > fuzzy.log

# Always update
$(DPO)/%.po: $(DPO)/templates.pot FORCE
	@$(call check-command, msgmerge, gettext)
	$(UPDATEPO) $(DPO)/$*.po $(DPO)/templates.pot
	$(DBIN)/fuzzypo $(DPO)/$*.po >>fuzzy.log

FORCE:

# source XML files for all languages (build prcess requires these)
SRC_XML	:=	$(addsuffix .xml, $(addprefix  $(MANUAL)., $(LANGALL)))
.PHONY: xml
xml: $(SRC_XML)

$(MANUAL).%.xml: $(DPO)/%.po $(MANUAL).en.xml
	@$(call check-command, po4a-translate, po4a)
	@$(call check-command, msgcat, gettext)
	$(DBIN)/genxml $*


#######################################################################
# Phase 8 build : Formatted conversion from XML
#######################################################################

#######################################################################
# $ make css       # update CSS and DIMG in $(TMP_DIR_DR)
#######################################################################
.PHONY: css
css:
	-rm -rf $(TMP_DIR_DR)/images
	mkdir -p $(TMP_DIR_DR)/images
	cp -f $(DXSL)/$(MANUAL).css $(TMP_DIR_DR)/$(MANUAL).css
	echo "AddCharset UTF-8 .txt" > $(TMP_DIR_DR)/.htaccess
	#cd $(DIMG) ; cp caution.png home.png important.png next.png note.png prev.png tip.png up.gif warning.png $(TMP_DIR_DR)/images
	cd $(DIMG) ; cp caution.png important.png note.png tip.png up.gif warning.png $(TMP_DIR_DR)/images
	cd png ; cp home.png next.png prev.png $(TMP_DIR_DR)/images

#######################################################################
# $ make html      # update all HTML in $(TMP_DIR_DR)
#######################################################################
.PHONY: html
html:	$(foreach LX, $(LANGALL), $(TMP_DIR_DR)/index.$(LX).html)

$(TMP_DIR_DR)/index.%.html: $(MANUAL).%.xml
	@$(call check-command, xsltproc, xsltproc)
	-mkdir -p $(TMP_DIR_DR)
	$(XPINC)   --stringparam base.dir $(TMP_DIR_DR)/ \
                --stringparam html.ext .$*.html \
                $(DXSL)/style-html.xsl $<

#######################################################################
# $ make txt       # update all Plain TEXT in $(TMP_DIR_DR)
#######################################################################
.PHONY: txt
txt:	$(foreach LX, $(LANGALL), $(TMP_DIR_DR)/$(MANUAL).$(LX).txt.gz)

# style-txt.xsl provides work around for hidden URL links by appending them explicitly.
$(TMP_DIR_DR)/$(MANUAL).%.txt.gz: $(MANUAL).%.xml
	@$(call check-command, w3m, w3m)
	@$(call check-command, xsltproc, xsltproc)
	-mkdir -p $(TMP_DIR_DR)
	@test -n "`which w3m`"  || { echo "ERROR: w3m not found. Please install the w3m package." ; false ;  }
	$(XPINC) $(DXSL)/style-txt.xsl $< | LC_ALL=en_US.UTF-8 \
	  w3m -o display_charset=UTF-8 -cols 70 -dump -no-graph -T text/html | \
	  gzip -n -9 - > $@

#######################################################################
# $ make pdf       # update all PDF in $(TMP_DIR_DR)
#######################################################################
.PHONY: pdf
pdf:	$(foreach LX, $(LANGALL), $(TMP_DIR_DR)/$(MANUAL).$(LX).pdf)

nopdf: $(TMP_DIR_DR)/nopdf.pdf

$(TMP_DIR_DR)/nopdf.pdf: nopdf.tex
	-mkdir -p $(CURDIR)/tmp
	cd "$(CURDIR)/tmp/"; \
	xelatex ../nopdf.tex

$(foreach LX, $(NOPDF), $(TMP_DIR_DR)/$(MANUAL).$(LX).pdf): $(TMP_DIR_DR)/nopdf.pdf
	-mkdir -p $(TMP_DIR_DR)
	cp $(TMP_DIR_DR)/nopdf.pdf $@

# dblatex.xsl provide work around for hidden URL links by appending them explicitly.
$(TMP_DIR_DR)/$(MANUAL).%.pdf: $(MANUAL).%.xml
	@$(call check-command, dblatex, dblatex)
	@$(call check-command, xsltproc, xsltproc)
	-mkdir -p $(CURDIR)/tmp
	@test -n "`which $(DBLATEX)`"  || { echo "ERROR: dblatex not found. Please install the dblatex package." ; false ;  }
	export TEXINPUTS=".:"; \
	export TMP_DIR_DR="$(CURDIR)/tmp/"; \
	$(XPINC) $(DXSL)/dblatex.xsl $<  | \
	$(DBLATEX) --style=native \
		--debug \
		--backend=xetex \
		--xsl-user=$(DXSL)/user_param.xsl \
		--xsl-user=$(DXSL)/xetex_param.xsl \
		--param=draft.mode=$(DRAFTMODE) \
		--param=lingua=$* \
		--output=$@ - || { echo "OMG!!!!!! XXX_CHECK_XXX ... Do not worry ..."; true ; }

#######################################################################
# $ make tex       # update all TeX source in $(TMP_DIR_DR)
#######################################################################
.PHONY: tex
tex:	$(foreach LX, $(LANGALL), $(TMP_DIR_DR)/$(MANUAL).$(LX).tex)

# dblatex.xsl provide work around for hidden URL links by appending them explicitly.
$(TMP_DIR_DR)/$(MANUAL).%.tex: $(MANUAL).%.xml
	-mkdir -p $(CURDIR)/tmp
	@test -n "`which $(DBLATEX)`"  || { echo "ERROR: dblatex not found. Please install the dblatex package." ; false ;  }
	export TEXINPUTS=".:"; \
	export TMP_DIR_DR="$(CURDIR)/tmp/"; \
	$(XPINC) $(DXSL)/dblatex.xsl $<  | \
	$(DBLATEX) --style=native \
		--debug \
		--type=tex \
		--backend=xetex \
		--xsl-user=$(DXSL)/user_param.xsl \
		--xsl-user=$(DXSL)/xetex_param.xsl \
		--param=draft.mode=$(DRAFTMODE) \
		--param=lingua=$* \
		--output=$@ - || { echo "OMG!!!!!! XXX_CHECK_XXX ... Do not worry ..."; true ; }

#######################################################################
# $ make epub      # update all epub in $(TMP_DIR_DR)
#######################################################################
.PHONY: epub
epub:	$(foreach LX, $(LANGALL), $(TMP_DIR_DR)/$(MANUAL).$(LX).epub)

$(TMP_DIR_DR)/$(MANUAL).%.epub: $(MANUAL).%.xml
	@$(call check-command, xsltproc, xsltproc)
	-mkdir -p $(TMP_DIR_DR)/$*/
	cd $(TMP_DIR_DR)/$*/ ; $(XPINC) $(CURDIR)/$(DXSL)/style-epub.xsl $(CURDIR)/$<
	cp -f $(DXSL)/mimetype $(TMP_DIR_DR)/$*/mimetype
	cp -f $(DXSL)/debian-reference.css $(TMP_DIR_DR)/$*/OEBPS/debian-reference.css
	cp -f $(DXSL)/debian-openlogo.png $(TMP_DIR_DR)/$*/OEBPS/debian-openlogo.png
	cd $(TMP_DIR_DR)/$*/ ; zip -r $@ ./

#######################################################################
# Phase 9 post/pre build: clean and distclean
#######################################################################
#######################################################################
# $ make clean     # clean files ready for tar 
#######################################################################
.PHONY: clean
clean:
	# CLEAN
	-rm -f *.swp *~ *.tmp
	-rm -f $(DPO)/*~ $(DPO)/*.mo $(DPO)/*.po.*
	-rm -f $(DASC)/*~
	-rm -rf tmp debian/tmp $(PUBLISHDIR)/$(MANUAL)
	-rm -f $(addsuffix .xml, $(addprefix $(MANUAL)., $(LANGALL)))
	-rm -f $(MANUAL).en.xml $(MANUAL).en.xmlt header.txt
	-rm -f $(REMOTE_DATA)

#######################################################################
# $ make distclean # clean files to reset RAWXML/ENT/POT
#######################################################################
.PHONY: distclean
distclean: clean
	# DISTCLEAN
	-rm -f $(MANUAL).raw.xml
	-rm -f $(ENT_STAT)
	-rm -f $(DPO)/*.pot
	-rm -f fuzzy.log
	-rm -f $(DBIN)/replace_package

#######################################################################
### Utility targets
#######################################################################

#######################################################################
# $ make wrap       # wrap all PO
#######################################################################
.PHONY: wrap nowrap wip
wrap:
	@$(call check-command, msgcat, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGCAT) -o $$XX $$XX ;\
	done
nowrap:
	@$(call check-command, msgcat, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGCAT) -o $$XX --no-wrap $$XX ;\
	done

wip:
	@$(call check-command, msgattrib, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGATTR) -o $$XX.fuzz --fuzzy        $$XX ;\
	$(MSGATTR) -o $$XX.untr --untranslated $$XX ;\
	done

#######################################################################
# $ make rsync     
# export build result to http://people.debian.org/~osamu/debian-reference/
#######################################################################
.PHONY: rsync
rsync: all
	rsync -avz $(TMP_DIR_DR)/ osamu@people.debian.org:public_html/debian-reference/

#######################################################################
# $ make url       # check duplicate URL references
#######################################################################
.PHONY: url
url: $(MANUAL).en.xml
	@echo "----- Duplicate URL references (start) -----"
	-sed -ne "/^<\!ENTITY/s/<\!ENTITY \([^ ]*\) .*$$/\" \1 \"/p"  < $< | uniq -d | xargs -n 1 grep $< -e  | grep -e "^<\!ENTITY"
	@echo "----- Duplicate URL references (end) -----"

#######################################################################
# Translate all
#######################################################################
$(DPO)/wikipedia.%.pot: $(DPO)/wikipedia.list
	$(DBIN)/interwiki $* "PRINT" < $< > $@

#######################################################################
# Translate untranslated
#######################################################################
$(DPO)/wikipedia.%.po: $(DPO)/wikipedia.%.list
	$(DBIN)/interwiki $* "NO"   < $< > $@


